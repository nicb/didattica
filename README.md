# Didattica

[![Join the chat at https://gitter.im/nicb/didattica](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/nicb/dispense?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Collezione di materiali didattici per il corso COME/02
(Composizione Musicale Elettroacustica).
