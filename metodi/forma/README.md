# Esercizi sul senso della forma musicale (TODO)

* I TR
  * percepire la forma
    * segmentare la forma (brani pop, Beatles, ecc.)
    * segmentare la forma (danze rinascimentali -> suite, poi da riprendere con la suite op.25 di Schoenberg)
    * segmentare la forma (Mozart variazioni *Ah vous dirais-je*)
    * segmentare la forma (forma sonata classica Beethoven *Waldstein*, primo movimento)
    * segmentare la forma (forma sonata brahmsiana Brahms Quintetto, primo movimento)
    * segmentare la forma (forma sonata Chopin Terza sonata, primo movimento)
  * estrarre caratteristiche formali
    * modulazioni armoniche
      * punto di vista classico
      * Beethoven
      * Brahms
      * Mahler
    * carattere agogico
      * Beethoven (op.59 n.1 Razumovsky)
    * carattere melodico
      * Beethoven (op.59 n.1 Razumovsky)
* II TR
  * utilizzare la forma per la composizione personale
    * riprendere forme classiche e re-interpretarle
    * contemporanei che re-interpretano: Bartok => Quartetti, Webern Variations op.27
* III TR
