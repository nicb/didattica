# Orecchio Interno

Qui albergano una serie di esercizi sperimentali e metodi da applicare al
Triennio di COME/02 per sviluppare l'orecchio interno.

## Possibili *démarches*

* Elaborazione del concetto di fusione
  * suoni fusi
  * fissione (per sfasamento, per vibrato, ecc.)
* Copia dal vero:
  * effetti
  * passaggi di musica elettronica (*Gesang*, drone del *Kathinka's Gesang*,
    *THEMA* "Blblblblooming"
    ecc.)

**TODO**: specificare meglio.
